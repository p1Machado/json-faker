package map_file_tree;

import java.io.FileWriter;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import io.bloco.faker.Faker;

public class MapFileTreeFaker {

	static int count = 0;

	static int mapCount = 0;

	static FileWriter file;

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		Faker faker = new Faker();
		ExecutorService executor = Executors.newFixedThreadPool(4);
		try {
			file = new FileWriter("./map-file-trees.json"); // try opening the file
			for (int i = 0; i < 300000; i++) {
				Runnable worker = new Runnable() {

					@Override
					public void run() {
						JSONObject obj = new JSONObject();
						obj.put("name", faker.company.name());
						obj.put("clientId", UUID.randomUUID().toString());
						JSONArray folders = new JSONArray();
						for (int x = 0; x < ThreadLocalRandom.current().nextInt(50, 260); x++) {
							JSONObject folder = new JSONObject();
							folder.put("name", faker.address.city());
							folder.put("mapId", UUID.randomUUID().toString());
							folder.put("fileTreeId", UUID.randomUUID().toString());
							folders.add(folder);
						}
						obj.put("mapFolders", folders);
						incrementMapCount(folders.size());
						try {
							write(obj, file);
						} catch (IOException e) {
							System.exit(0);
						}
					}
				};

				executor.execute(worker);
			}

			executor.shutdown();
			while (!executor.isTerminated()) {
			}
			System.out.println("Finished all threads");
			System.out.println("Generated " + mapCount + " maps");
			
			file.flush();
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	static synchronized void write(JSONObject obj, FileWriter file) throws IOException {
		file.write(obj.toJSONString());
		System.out.println(++count);
	}

	static synchronized void incrementMapCount(int ammount) {
		mapCount += ammount;
	}

}
